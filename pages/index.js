import Link from "next/link";

import {
  Container,
  Text,
  Box,
  Button,
  Icon,
  VStack,
  HStack,
  // Flex,
  // Center,
} from "@chakra-ui/react";

import { keyframes, usePrefersReducedMotion } from "@chakra-ui/react";
import { FaHeart, FaAngleRight } from "react-icons/fa";

const Logo = () => {
  // double heartbeat animation
  const heartbeat = keyframes`
    0% {
      transform: scale(1);
    }
    14% {
      transform: scale(1.1);
    }
    28% {
      transform: scale(1);
    }
    42% {
      transform: scale(1.1);
    }
  `;

  const prefersReducedMotion = usePrefersReducedMotion();

  // heartbeat animation
  const animation = prefersReducedMotion
    ? undefined
    : `${heartbeat} 1.5s ease-in-out infinite 1s`;

  return (
    <>
      <Box p={8}>
        <Icon
          as={FaHeart}
          w={120}
          h={120}
          color="red.500"
          animation={animation}
        />
      </Box>
      {/* <Center w="100%" h={200} animation={animation}>
        <Box p={8} filter="auto" blur="40px" position={"absolute"} zIndex={0}>
          <Icon as={FaHeart} w={120} h={120} color="red.500" />
        </Box>
        <Box p={8} zIndex={1} position="absolute">
          <Icon as={FaHeart} w={120} h={120} color="red.500" />
        </Box>
      </Center> */}
    </>
  );
};

export default function Home() {
  return (
    <Container maxW="container.md" centerContent>
      <Logo />
      <VStack spacing={3}>
        <Text
          color="current"
          fontSize="6xl"
          fontWeight="extrabold"
          textAlign="center"
        >
          I'm Tactile
        </Text>
        <Text
          color="current"
          fontSize="2xl"
          fontWeight="bold"
          textAlign="center"
        >
          A place to share your touches over distance
        </Text>
        <br />
        <HStack spacing={4}>
          <Link href="https://youtu.be/dQw4w9WgXcQ">
            <Button size="lg" fontWeight="bold">
              Get Started
            </Button>
          </Link>
          <Link href="/login">
            <Button size="lg" fontWeight="bold" colorScheme="teal">
              Login
              <Icon as={FaAngleRight} ml={2} size="sm" />
            </Button>
          </Link>
        </HStack>
      </VStack>
    </Container>
  );
}
