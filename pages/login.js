import { Container, Text, Button } from "@chakra-ui/react";
import { collection, orderBy, query, where } from "firebase/firestore";
import { useCollectionData } from "react-firebase-hooks/firestore";
import Link from "next/link";

import { useFirebase } from "@/components/FirebaseContext";

function ListUsers() {
  const { db } = useFirebase();
  const q = collection(db, "users");

  // const q = query(collection(db, "users"), where("username", "==", "potat"));
  const [value, loading, error] = useCollectionData(q, {
    snapshotListenOptions: {
      includeMetadataChanges: true,
    },
  });

  return (
    <ul>
      {loading && <li>Loading...</li>}
      {error && <li>Error: {JSON.stringify(error)}</li>}
      {value &&
        value.map((doc) => (
          <li key={doc.id}>
            <Link href={`/${doc.username}`}>{doc.name}</Link>
          </li>
        ))}
    </ul>
  );
}

export default function Home() {
  const { user, signIn } = useFirebase();

  return (
    <Container maxW="container.md" centerContent>
      <Text
        color="current"
        fontSize="xl"
        fontWeight="extrabold"
        textAlign="center"
        m={3}
      >
        Login or Register
      </Text>
      <div>
        {!user && <Button onClick={signIn}>Sign in with Google</Button>}
        <br />
        {user && (
          <div>
            <Text fontSize="xl" my={3}>
              Users
            </Text>
            <br />
            <ListUsers />
          </div>
        )}
      </div>
    </Container>
  );
}
