import { Container, Text } from "@chakra-ui/react";

import { useRouter } from "next/router";

export default function Home() {
  // get [uid] from the URL
  const { uid } = useRouter().query;

  return (
    <Container maxW="container.xl">
      <Text>Friends of {uid}</Text>
    </Container>
  );
}
