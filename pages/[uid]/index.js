import { Container, Text, Highlight } from "@chakra-ui/react";
import { SendTouchBtn } from "@/components/SendTouchBtn";

import { useRouter } from "next/router";

export default function Home() {
  // get [uid] from the URL
  const router = useRouter();
  const { uid } = router.query;

  // TODO: get user data from firestore using uid
  // while loading, show "Loading..."
  // const q = query(collection(db, "users"), where("username", "==", "potat"));

  if (router.isFallback || !uid) {
    return <Text>Loading...</Text>;
  }

  return (
    <Container maxW="container.md" centerContent>
      <Text fontSize="xl">
        <Highlight
          query={[uid]}
          styles={{ px: "2", py: "1", rounded: "full", bg: "teal.100" }}
        >
          {`Dashboard for ${uid}`}
        </Highlight>
      </Text>
      <SendTouchBtn />
    </Container>
  );
}
