import { Container, Text, Button, VStack } from "@chakra-ui/react";
import Link from "next/link";

export default function Home() {
  return (
    <Container maxW="container.md" centerContent>
      <VStack spacing={3}>
        <Text
          color="current"
          fontSize="6xl"
          fontWeight="extrabold"
          textAlign="center"
        >
          I'm Tactile
        </Text>
        <Text
          color="current"
          fontSize="2xl"
          fontWeight="extrabold"
          textAlign="center"
        >
          A place to share your touches over distance
        </Text>
        <br />
        <Link href="https://youtu.be/dQw4w9WgXcQ">
          <Button size="lg" fontWeight="extrabold" textAlign="center">
            Get Started
          </Button>
        </Link>
      </VStack>
    </Container>
  );
}