import "@/styles/globals.css";

import { ChakraProvider, Flex } from "@chakra-ui/react";
import { AppBar } from "@/components/AppBar";
import { Container } from "@chakra-ui/react";

import { FirebaseProvider } from "@/components/FirebaseContext";
import firebaseConfig from "@/config/firebase";
import theme from "@/config/theme";

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <FirebaseProvider config={firebaseConfig}>
        <Container maxW="container.md" px={3}>
          <Flex h="100vh" direction="column">
            <AppBar />
            <Component {...pageProps} />
          </Flex>
        </Container>
      </FirebaseProvider>
    </ChakraProvider>
  );
}

export default MyApp;
