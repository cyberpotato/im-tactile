import { Container, Text } from "@chakra-ui/react";

export default function Home() {
  return (
    <Container maxW="container.md" centerContent>
      <Text>My dashboard</Text>
    </Container>
  );
}
