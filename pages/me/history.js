import { Container, Text } from "@chakra-ui/react";

export default function Home() {
  return (
    <Container maxW="container.xl">
      <Text>My history</Text>
    </Container>
  );
}
