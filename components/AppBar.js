import Link from "next/link";

import {
  Avatar,
  Box,
  Text,
  Icon,
  HStack,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuGroup,
  Tooltip,
} from "@chakra-ui/react";

import {
  FaHeart,
  FaUser,
  FaCog,
  FaSignOutAlt,
  FaSignInAlt,
} from "react-icons/fa";

import { ColorModeSwitcher } from "@/components/ColorModeSwitcher";
import { useFirebase } from "./FirebaseContext";

const UserMenu = () => {
  const { user, signIn, signOut } = useFirebase();

  return (
    <Menu>
      <Tooltip label="Account Menu" placement="bottom-end" openDelay={500}>
        <MenuButton
          as={Avatar}
          size="sm"
          bg="gray.500"
          src={user ? user.photoURL : undefined}
        />
      </Tooltip>
      <MenuList>
        {user ? (
          <MenuGroup title={`Profile • ${user.displayName}`}>
            <MenuItem icon={<FaUser />}>Friends</MenuItem>
            <MenuItem icon={<FaCog />}>Settings</MenuItem>
            <MenuItem icon={<FaSignOutAlt />} onClick={signOut}>
              Sign Out
            </MenuItem>
          </MenuGroup>
        ) : (
          <MenuGroup title="Profile">
            <MenuItem icon={<FaSignInAlt />} onClick={signIn}>
              Sign In
            </MenuItem>
          </MenuGroup>
        )}
      </MenuList>
    </Menu>
  );
};

export const AppBar = () => {
  return (
    <HStack p={3} spacing={3} my={3}>
      <Tooltip label="Best App Ever!" placement="bottom" openDelay={500}>
        <Link href="/">
          <HStack spacing={4}>
            <Icon as={FaHeart} h={6} w={6} />
            <Text fontSize="xl" fontWeight="extrabold">
              I'm Tactile
            </Text>
          </HStack>
        </Link>
      </Tooltip>
      <Box flexGrow={1} />
      <ColorModeSwitcher />
      <UserMenu />
    </HStack>
  );
};
