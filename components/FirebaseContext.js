import { useState, createContext, useContext, useMemo } from "react";

import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { useAuthState } from "react-firebase-hooks/auth";
import { getFirestore } from "firebase/firestore";

import {
  useCollectionData,
  useCollection,
} from "react-firebase-hooks/firestore";

import {
  collection,
  doc,
  setDoc,
  query,
  where,
  getDocs,
} from "firebase/firestore";

const FirebaseContext = createContext();

export function useFirebase() {
  return useContext(FirebaseContext);
}

export function FirebaseProvider({ config, children }) {
  // create a new firebase app
  const app = initializeApp(config);
  // create a new firebase auth instance
  const auth = getAuth(app);
  // get the current user
  const [user] = useAuthState(auth);
  // create a new firebase firestore instance
  const db = getFirestore(app);
  // create auth provider
  const provider = new GoogleAuthProvider();

  // sign in with google
  const signIn = async () => {
    const result = await signInWithPopup(auth, provider);
    if (result.user) {
      // check if user exists in firestore
      const usersRef = collection(db, "users");
      const q = query(usersRef, where("uid", "==", result.user.uid));
      const userDoc = await getDocs(q);
      // if user does not exist, add user to firestore
      if (userDoc.empty) {
        await setDoc(doc(db, "users", result.user.uid), {
          uid: result.user.uid,
          name: result.user.displayName,
          email: result.user.email,
          photo: result.user.photoURL,
          username: createUsername(result.user),
        });
        alert("User added");
        // console.log("User added");
        // TODO: redirect to profile page
      } else {
        // alert("User already exists");
        console.log("User already exists");
      }
    }
  };

  // sign out
  const signOut = () => {
    auth.signOut();
  };

  const value = useMemo(() => {
    return { app, auth, user, db, signIn, signOut };
  }, [app, auth, user, db, signIn, signOut]);

  return (
    <FirebaseContext.Provider value={value}>
      {children}
    </FirebaseContext.Provider>
  );
}

// create a username from the user's display name
// allowed characters: a-z, A-Z, 0-9, . and _
// TODO: check if username already exists

function createUsername(user) {
  const nick = user.displayName.toLowerCase().replace(/[^a-z0-9_.]/g, "");
  return nick.length > 0 ? nick : user.email.split("@")[0];
}
