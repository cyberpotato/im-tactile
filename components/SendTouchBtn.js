import {
  VStack,
  Text,
  Highlight,
  IconButton,
  Button,
  ButtonGroup,
  Input,
  HStack,
} from "@chakra-ui/react";

import { FaHeart, FaPaperPlane } from "react-icons/fa";
import { EditIcon } from "@chakra-ui/icons";
import { useState } from "react";

import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react";

export const SendTouchBtn = () => {
  const [clicks, setClicksBase] = useState(0);
  const [message, setMessage] = useState("");

  const handleChange = (event) => {
    if (event.key === "Enter") {
      sendTouches();
    } else {
      setMessage(event.target.value);
    }
  };

  const setClicks = (newClicks) => {
    setClicksBase(newClicks < 0 ? 0 : newClicks);
  };

  const sendTouches = () => {
    if (clicks > 0) {
      alert(
        `Sending ${clicks} touches with ` +
          (message ? `message: ${message}` : "no message")
      );
      setClicks(0);
      setMessage("");
    }
  };

  const ClicksPopover = ({ children }) => {
    return clicks > 0 ? (
      <Popover>
        <PopoverTrigger>{children}</PopoverTrigger>
        <PopoverContent>
          <PopoverArrow />
          <PopoverCloseButton />
          <PopoverHeader>Remove touches</PopoverHeader>
          <PopoverBody>
            You can remove some or all of your touches by clicking the buttons
            below
          </PopoverBody>
          <PopoverFooter display="flex" justifyContent="flex-end">
            <ButtonGroup size="sm">
              <Button colorScheme="gray" onClick={() => setClicks(clicks - 1)}>
                -1
              </Button>
              <Button colorScheme="gray" onClick={() => setClicks(clicks - 10)}>
                -10
              </Button>
              <Button colorScheme="red" onClick={() => setClicks(0)}>
                Remove all
              </Button>
            </ButtonGroup>
          </PopoverFooter>
        </PopoverContent>
      </Popover>
    ) : (
      children
    );
  };

  return (
    <VStack p={12} spacing={6} centerContent>
      <IconButton
        aria-label="Send Touch"
        icon={<FaHeart size={200} />}
        height={200}
        width={200}
        // colorScheme="red"
        variant="ghost"
        color="red.400"
        _hover={{
          color: "red.500",
          // hover zoom animation
          transform: "scale(1.1)",
          transition: "transform 0.2s",
        }}
        _active={{
          color: "red.400",
          transform: "scale(0.9)",
          transition: "transform 0.2s",
        }}
        _focus={{
          WebkitTapHighlightColor: "transparent",
        }}
        onClick={() => setClicks(clicks + 1)}
      />
      <ClicksPopover>
        <HStack spacing={1}>
          <Text fontSize="lg" p={2}>
            <Highlight
              query={`x${clicks}`}
              styles={{ px: "2", py: "1", rounded: "full", bg: "pink.200" }}
            >
              {clicks === 0 ? "Click to Send Touch" : `Send Touch x${clicks}`}
            </Highlight>
          </Text>
          {clicks > 0 && <EditIcon />}
        </HStack>
      </ClicksPopover>
      <Input
        placeholder="Enter a message"
        value={message}
        onChange={handleChange}
        onKeyPress={handleChange}
      />
      <Button
        rightIcon={<FaPaperPlane />}
        colorScheme={clicks === 0 ? "gray" : "telegram"}
        onClick={() => sendTouches()}
        disabled={clicks === 0}
      >
        Send
      </Button>
    </VStack>
  );
};
