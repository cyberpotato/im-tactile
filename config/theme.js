import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  styles: {
    global: {
      body: {
        transitionProperty: "all",
        transitionDuration: "normal",
      },
    },
  },
  config: {
    disableTransitionOnChange: false,
    initialColorMode: "dark",
    useSystemColorMode: true,
  },
});

export default theme;
